﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upsword : MonoBehaviour {
	private Vector3 initialPosition;

	// Use this for initialization
	void Start () {
		initialPosition = transform.position;
	}

	// Update is called once per frame
	void Update () {
		float height = Mathf.Sin (Time.time*2.0f) * 5.0f;
		Vector3 moveblock = this.gameObject.transform.position;
		moveblock.y = height + initialPosition.y;
		this.gameObject.transform.position = moveblock;

	}
}
