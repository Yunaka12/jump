﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgainCoins : MonoBehaviour {
	
	public float minHeight;
	public float maxHeight;
	public GameObject coin;
	public int coinget;
	GameObject child;
	GameObject grandchild;
	// Use this for initialization
	void Start () {
		child = gameObject.transform.Find("coin").gameObject;
		grandchild = child.transform.Find ("colcoin").gameObject;
	}



	void OnScrollEnd(){
		ChangeHeight ();
	}

	void ChangeHeight(){
		grandchild.SendMessage ("Stop");
		child.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f); //rotateに値を代入
		coin.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

		float height = Random.Range (minHeight, maxHeight);
		Vector3 gp =  coin.transform.position;
		gp.y = height;
		coin.transform.position = gp;

	}


	// Update is called once per frame
	void Update () {
		
	}
}
