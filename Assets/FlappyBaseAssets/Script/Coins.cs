﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coins : MonoBehaviour {


	GameObject gameController;
	GameObject  parent;
	GameObject grandparent;
	AudioSource audio;


	public AudioClip get;
	public int coinget = 0;
	// Use this for initialization
	void Start () {
		parent = gameObject.transform.parent.gameObject;
		grandparent = parent.transform.parent.gameObject;
		gameController = GameObject.FindWithTag ("GameController");	
		audio = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
		if (coinget == 1) {
			parent.transform.Rotate (30, 20, 0);
				
		}
	}



	void OnTriggerEnter2D(Collider2D other)
	{
		if (coinget == 1)
			return;
		if(other.gameObject.tag == "azarashi"){
			
			audio.PlayOneShot (get, 1.0f);
			gameController.SendMessage ("IncreaseScore");
			coinget = 1;
			Invoke ("delay", audio.clip.length);

		}
			


	}


	void delay(){
		parent.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

	}

	void Stop(){
		coinget = 0;
	}

}
