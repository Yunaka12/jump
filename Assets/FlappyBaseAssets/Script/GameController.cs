﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	enum State{
		Ready,
		Play,
		GameOver,
		GameClear
	}

	State state;

	public int score;
	public AzarashiController azarashi;
	public GameObject blocks;
	public GameObject coins;
	public GameObject karasu;
	public GameObject block;
	public GameObject clear;
	public Text scoreLabel;
	public Text stateLavel;
	public bool gameclear;

	// Use this for initialization
	void Start () {
		Ready ();
		
	}

	void LateUpdate(){
		switch (state) {
		case State.Ready:
			if (Input.GetButtonDown ("Fire1"))
				GameStart ();
			break;
		case State.Play:
			if (azarashi.IsDead ())
				GameOver ();
			else if (gameclear == true)
				Finish ();
			break;
		case State.GameOver:
			if (Input.GetButtonDown ("Fire1"))
				Reload ();
			break;

		}
	}

	void Ready(){
		state = State.Ready;
		azarashi.SetSteerActive (false);
		blocks.SetActive (false);
		coins.SetActive (false);
		karasu.SetActive (false);
		block.SetActive (false);
		clear.SetActive (false);
		scoreLabel.text = "Score : " + 0;

		stateLavel.gameObject.SetActive (true);
		stateLavel.text = "Ready";
	}

	void GameStart(){
		state = State.Play;
		azarashi.SetSteerActive (true);
		//blocks.SetActive (true);
		coins.SetActive (true);
		karasu.SetActive (true);
		azarashi.Flap ();

		stateLavel.gameObject.SetActive (false);
		stateLavel.text = "";
		StartCoroutine("Clear");


	}

	void GameOver(){
		state = State.GameOver;

		ScrollObject[] scrollObjects = GameObject.FindObjectsOfType<ScrollObject> ();
		foreach (ScrollObject so in scrollObjects)
			so.enabled = false;

		stateLavel.gameObject.SetActive (true);
		stateLavel.text ="GameOver";

	}

	void Finish(){

		block.SetActive (true);
	}

	IEnumerator Clear(){
		
		yield return new WaitForSeconds(50.0f);
		if (state == State.Play) {
			gameclear = true;
		}

	}
		


	void Reload()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	public void IncreaseScore()
	{
		
		score++;

		scoreLabel.text = "Score : " + score;

	}

	public int GetScore()
	{
		return score;
	}


	
	// Update is called once per frame
	void Update () {
		
	}
}
