﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinClear : MonoBehaviour {

	public GameObject clear;
	public AzarashiController azarashi;
	public GameObject coins;
	public GameObject karasu;
	public GameObject block;

	// Use this for initialization
	void Start () {
		clear.SetActive (false);
	}

	void OnTriggerExit2D(Collider2D other)
	{

		if (other.gameObject.tag == "azarashi") {
			clear.SetActive (true);
			azarashi.SetSteerActive (false);
			coins.SetActive (false);
			karasu.SetActive (false);
			block.SetActive (false);
		}


	}

	// Update is called once per frame
	void Update () {

	}
}
