﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AzarashiController : MonoBehaviour {
	GameObject _child;
	Rigidbody2D rb2D;
	Animator animator;
	float angle;
	bool isDead;
	AudioSource audio;
	float groundflap = 16;
	float freezeposx=0;

	public float maxHeight;
	public float flapVelocity;
	public float relativeVelocityX;
	public GameObject sprite;
	public AudioClip pyon;
	public AudioClip poka;
	public AudioClip syun;
	public int life = 3;
	public GameObject heart3;
	public GameObject heart2;
	public GameObject heart1;
	public int updown;
	public bool mousedown;



	public bool IsDead(){
		return isDead;
		//脂肪
	}

	void Awake()
	{
		
		rb2D = GetComponent<Rigidbody2D> ();
		animator = sprite.GetComponent<Animator> ();

	}



	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		freezeposx = this.gameObject.transform.position.x; //カラスとかと当たったときにアザラシを移動させないように。
		_child = GameObject.FindWithTag ("azarashi");

	}
	
	// Update is called once per frame
	void Update () {
		Vector3 gp =  this.gameObject.transform.position;
		gp.x = freezeposx;
		this.gameObject.transform.position = gp;

		if (Input.GetButtonDown ("Fire1") && transform.position.y < maxHeight) {
			Flap ();
		}

		if (Input.GetMouseButtonDown (1)) {
			
			mousedown = true;
			_child.GetComponent<SpriteRenderer> ().color = new Color (1.0f, 1.0f, 0.0f, 1.0f);
			StartCoroutine("Out");
		}

		if (Input.GetMouseButtonUp (1)||updown==1) {
			
			mousedown = false;
			_child.GetComponent<SpriteRenderer> ().color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
			updown = 0;
		}

		ApplyAngle ();

		animator.SetBool ("flap", angle >= 0.0f);


	}

	public void Flap()
	{
		if (isDead)	return;
		if (rb2D.isKinematic)	return;

		rb2D.velocity = new Vector2 (0.0f, flapVelocity);
		audio.PlayOneShot(pyon, 0.7f);

	}

	void ApplyAngle(){
		float targetAngle;

		if (isDead) {
			targetAngle = -90.0f;

		} else {


			targetAngle = Mathf.Atan2 (rb2D.velocity.y, relativeVelocityX) * Mathf.Rad2Deg;
		}
		angle = Mathf.Lerp (angle, targetAngle, Time.deltaTime * 10.0f);

		sprite.transform.localRotation = Quaternion.Euler (0.0f, 0.0f, angle);
	}

	IEnumerator Out(){
		
		yield return new WaitForSeconds(0.2f);
		updown = 1;

	}

	void OnCollisionEnter2D(Collision2D collision){
		if (isDead)
			return;
		
		if (!mousedown) {
			Camera.main.SendMessage ("Clash");
			life = life - 1;

			if (life == 2) {
				heart3.SetActive (false);
			}

			if (life == 1) {
				heart2.SetActive (false);
			}



			if (life == 0) {
				isDead = true;
				audio.PlayOneShot (poka, 1.0f);
				heart1.SetActive (false);
			}

			if (life != 0) {
				if (collision.gameObject.tag == "ground")
					rb2D.velocity = new Vector2 (0.0f, groundflap);
				//地面から空中に飛ばす。そうしないと、地面に当たり続けて一回地面にぶつかると死ぬ。ただし、3回目の時はそのまま死んでほしいのでnot0
			}
		} else if (mousedown) {
			audio.PlayOneShot (syun, 1.0f);
		}

	}






	public void SetSteerActive(bool active){
		rb2D.isKinematic = !active;
	}





	}



